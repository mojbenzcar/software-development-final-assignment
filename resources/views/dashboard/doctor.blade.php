<div class="panel panel-default">
	<div class="panel-heading">
		Welcome, {{auth()->user()->name}}
	</div>

	<div class="panel-body">
		Your have successfully logged in.

		@include('includes.roster')
	</div>
</div>