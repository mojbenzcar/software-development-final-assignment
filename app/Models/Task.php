<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $guarded = [];

    public function nurse()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
