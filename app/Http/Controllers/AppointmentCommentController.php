<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Models\AppointmentComment;
use App\Models\Patient;
use Illuminate\Http\Request;

class AppointmentCommentController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        return view('comment.create', compact('patient'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCommentRequest|Request $request
     *
     * @param Patient                      $patient
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCommentRequest $request, Patient $patient)
    {
        $data  ['comments'] = $request->comments;
        $data['patient_id'] = $patient->id;
        $data['user_id']    = auth()->user()->id;

        if ($request->has('appointment_id')) {
            $data['appointment_id'] = $request->appointment_id;
        }
        $comment = AppointmentComment::create($data);
        if (!$comment) {
            session()->flash('error', "Error creating Comment. Please contact tech department.");

            return redirect()->back();
        }
        session()->flash('success', "Comment Created Successfully");

        return redirect()->route('patient.show', $patient->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Patient            $patient
     * @param AppointmentComment $comment
     *
     * @return \Illuminate\Http\Response
     * @internal param \App\AppointmentComment|AppointmentComment $appointmentComment
     *
     */
    public function edit(Patient $patient, AppointmentComment $comment)
    {
        return view('comment.edit', compact('patient', 'comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request                  $request
     * @param Patient                                    $patient
     * @param \App\AppointmentComment|AppointmentComment $appointmentComment
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $patientId, $appointmentCommentId)
    {
        $appointmentComment = AppointmentComment::findOrFail($appointmentCommentId);
        $patient            = Patient::findOrFail($patientId);
        $data['comments']   = $request->comments;

        if ($request->has('appointment_id')) {
            $data['appointment_id'] = $request->appointment_id;
        }

        $comment = $appointmentComment->update($data);
        if (!$comment) {
            session()->flash('error', "Error updating Comment. Please contact tech department.");

            return redirect()->back();
        }
        session()->flash('success', "Comment updated Successfully");

        return redirect()->route('patient.show', $patient->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentComment $appointmentComment
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppointmentComment $appointmentComment)
    {

    }
}
