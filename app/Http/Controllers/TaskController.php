<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\User;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            $tasks = Task::all();
        

        return view('task.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $formData = [
            'users' => User::where('role_id', 4)
                           ->get()
                           ->pluck('name_email', 'id')
                           ->prepend(
                               'Please select',
                               ''
                           ),
        ];

        return view('task.create', $formData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = Task::create($request->all());
        if ($task) {
            session()->flash('success', "Task Created.");
        } else {
            session()->flash('error', "Error Creating Task.");
        }

        return redirect()->route('task.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $formData = [
            'task'  => $task,
            'users' => User::where('role_id', 4)
                           ->get()
                           ->pluck('name_email', 'id')
                           ->prepend(
                               'Please select',
                               ''
                           ),
        ];

        return view('task.edit', $formData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task                $task
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
         try {

            $task->update($request->all());

            session()->flash('success', "Task information edited Successfully.");

        } catch (\Exception $e) {
            session()->flash('error', "Error updating Task information.");
        }

        return redirect()->route('task.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
