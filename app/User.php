<?php

namespace App;

use App\Models\Role;
use App\Models\Roster;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'role_id',
        'password',
        'mobile_no',
        'address',
        'qualification',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function roster()
    {
        return $this->hasMany(Roster::class, 'user_id');
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getNameEmailAttribute()
    {
        return "{$this->name}({$this->email})- {$this->role->title}";
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = bcrypt($pass);
    }

    public function isAdmin()
    {
        return in_array(auth()->user()->role_id, [1]);
    }

    public function isReceptionist()
    {
        return in_array(auth()->user()->role_id, [2]);
    }

    public function isDoctor()
    {
        return in_array(auth()->user()->role_id, [3]);
    }

    public function isNurse()
    {
        return in_array(auth()->user()->role_id, [4]);
    }

    public function thisWeekRoster()
    {
        return $this->roster()->whereBetween(
            'date',
            [
                Carbon::parse('last monday')->startOfDay(),
                Carbon::parse('next sunday')->endOfDay(),
            ]
        );
    }
}
