<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    protected $guarded = [];

    public function todays()
    {
        $query = $this->whereDate(
            'date',
            '=',
            Carbon::today()->toDateString()
        );

        if (auth()->user()->isDoctor()) {
            $query->where('user_id', auth()->user()->id);
        }

        return $query;
    }

    public function upcoming()
    {
        $query = $this->whereDate(
            'date',
            '>',
            Carbon::today()->toDateString()
        );

        if (auth()->user()->isDoctor()) {
            $query->where('user_id', auth()->user()->id);
        }

        return $query;
    }

    public function past()
    {
        $query = $this->whereDate(
            'date',
            '<',
            Carbon::today()->toDateString()
        );
        if (auth()->user()->isDoctor()) {
            $query->where('user_id', auth()->user()->id);
        }

        return $query;
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function doctor()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
