@extends('layouts.app')
@section('page_title', "Nurse Task Management")
@section('content')
	{!! Form::open(['method' => 'POST', 'route' => ['task.store']]) !!}

	<div class="panel panel-default">
		<div class="panel-heading">
			Create New Patient
		</div>

		<div class="panel-body">
			@include('task.fields')
			{!! Form::submit("Create New Task", ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>

	</div>

@stop

