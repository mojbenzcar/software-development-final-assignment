<h2>Your Roster for this week.</h2>
<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>Sno</th>
		<th>Date</th>
		<th>Start Time</th>
		<th>End Time</th>
	</tr>
	</thead>

	<tbody>
	@forelse(auth()->user()->thisWeekRoster()->get() as $roster)
		<tr>
			<td>{{$roster->id}}</td>
			<td>{{$roster->date}} </td>
			<td>{{$roster->start_time}} </td>
			<td>{{$roster->end_time}}
			</td>
		</tr>
	@empty
		<tr>
			<td colspan="4">No Roster</td>
		</tr>
	@endforelse
	</tbody>
</table>