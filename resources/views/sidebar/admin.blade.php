<li>
	<a href="{{route('patient.index')}}"><i class="fa fa-user"></i><span class="title"> Patients</span></a>
</li>
<li>
	<a href="{{route('appointment.index')}}"> <i class="fa fa-calendar"></i> Appointment</a>
</li>
<li>
	<a href="{{route('user.index')}}"><i class="fa fa-users"></i> Staff</a>
</li>
<li>
	<a href="{{route('roster.index')}}"> <i class="fa fa-hourglass"></i> Roster</a>
</li>
<li>
	<a href="{{route('task.index')}}"> <i class="fa fa-tasks"></i> Tasks</a>
</li>
