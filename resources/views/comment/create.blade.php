@extends('layouts.app')
@section('page_title',"Review Patient Condition")
@section('content')
	<<a href="{{route('patient.show', $patient->id)}}">Back</a>
	{!! Form::open(['method' => 'POST', 'route' => ['patient.comment.store', $patient->id]]) !!}

	<div class="panel panel-default">
		<div class="panel-heading">
			Add Review for {{$patient->name}}
			@php
				use  App\Models\Appointment;
					if(request()->has('appointment_id')){
					$appointment = Appointment::findOrFail(request()->get('appointment_id'));
				}
			@endphp
			@if (request()->has('appointment_id'))
				Appointment: {{$appointment->date}} ({{$appointment->start_time}} - {{$appointment->end_time}} )
			@endif
		</div>

		<div class="panel-body">
			@include('comment.fields')
			{!! Form::submit('Add Comment', ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
	</div>

@stop
