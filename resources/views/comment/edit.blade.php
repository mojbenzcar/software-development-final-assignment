@extends('layouts.app')
@section('page_title', "Patient Management")
@section('content')
	{!! Form::model($comment, ['method' => 'PUT', 'route' => ['patient.comment.update', $patient->id, $comment->id]]) !!}
	<div class="panel panel-default">
		<div class="panel-heading">
			Edit Staff Profile
		</div>

		<div class="panel-body">
			@include('comment.fields')
			{!! Form::submit("Edit Comment", ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
	</div>

@stop

