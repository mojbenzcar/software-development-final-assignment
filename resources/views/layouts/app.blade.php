<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title', "Medicare System")</title>
	<meta name="description" content="@yield('meta_description', 'Medicare system')">
	@yield('meta')
	@include('includes.header_script')
</head>
<body>
<div id="wrapper">
	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

	@include('includes.header')

	@include('includes.sidebar')
	<!-- /.navbar-static-side -->
	</nav>

	<div id="page-wrapper">
		<div class="row">
			<h3 class="page-header">@yield('page_title')</h3>
			<div class="col-lg-12" style="padding-left: 0">
				@include('includes.messages')
				@yield('content')
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
	{{ csrf_field() }}
</form>
<!-- jQuery -->
<script src="{{url('vendor/jquery')}}/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{url('vendor/bootstrap/js')}}/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{url('vendor/metisMenu')}}/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="{{url('js')}}/sb-admin-2.js"></script>
<script src="{{url('vendor/datatables/js')}}/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            responsive: true
        });
    });
</script>
@yield('javascript')
</body>

</html>
