<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePatientRequest;
use App\Http\Requests\EditPatientRequest;
use App\Models\Patient;

class PatientController extends Controller
{
    /**
     * Display a listing of Patient.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::all();

        return view('patient.index', compact('patients'));
    }

    /**
     * Show the form for creating new Patient.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patient.create');
    }

    /**
     * Store a newly created Patient in storage.
     *
     * @param  \App\Http\Requests\CreatePatientRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePatientRequest $request)
    {
        $patient = Patient::create($request->all());
        if (!$patient) {
            session()->flash('error', "Error creating Patient. Please contact tech department.");

            return redirect()->back();
        }
        session()->flash('success', "Patient Created Successfully");

        return redirect()->route('patient.index');
    }


    /**
     * Show the form for editing Patient.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        $relations = [
            'patient' => $patient,
        ];


        return view('patient.edit', $relations);
    }

    /**
     * write brief description
     *
     * @param Patient            $patient
     * @param EditPatientRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Patient $patient, EditPatientRequest $request)
    {
        try {

            $patient->update($request->all());

            session()->flash('success', "Patient information edited Successfully.");

        } catch (\Exception $e) {
            session()->flash('error', "Error updating patient information.");
        }

        return redirect()->route('patient.index');
    }


    /**
     * Display Patient.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::findOrFail($id);

        return view('patient.show', compact('patient'));
    }


    /**
     * Remove Patient from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::findOrFail($id);
        $patient->delete();

        return redirect()->route('patient.index');
    }

}
