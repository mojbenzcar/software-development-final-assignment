<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('comments', 'Comments', ['class' => 'control-label']) !!}
		{!! Form::textarea('comments', old('comments'), ['class' => 'form-control ', 'placeholder' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('comments'))
			<p class="help-block">
				{{ $errors->first('comments') }}
			</p>
		@endif
	</div>
</div>