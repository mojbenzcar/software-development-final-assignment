<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['id' => 1, 'role' => 'admin', 'title' => 'Administrator',],
            ['id' => 2, 'role' => 'receptionist', 'title' => 'Receptionist',],
            ['id' => 3, 'role' => 'doctor', 'title' => 'Doctor',],
            ['id' => 4, 'role' => 'nurse', 'title' => 'Nurse',],
        ];

        foreach ($roles as $role) {
            //Role::create($role);
        }
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'mobile_no'          => 'admin@admin.com',
                'address'          => 'admin@admin.com',
                'qualification'          => 'admin@admin.com',
                'password'       => bcrypt('admin'),
                'role_id'        => 1,
                'remember_token' => '',
            ],
        ];

        foreach ($users as $user) {
            \App\User::create($user);
        }
    }
}
