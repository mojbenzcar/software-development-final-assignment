<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    protected $guarded = [];

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getIdentifierAttribute()
    {
        return "#{$this->id}-{$this->name}";
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'patient_id')->orderBy('id', 'desc');
    }

    public function comments(){
        return $this->hasMany(AppointmentComment::class, 'patient_id')->orderBy('id', 'desc');
    }
}
