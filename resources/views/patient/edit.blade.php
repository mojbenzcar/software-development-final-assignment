@extends('layouts.app')
@section('page_title', "Patient Management")
@section('content')
	{!! Form::model($patient, ['method' => 'PUT', 'route' => ['patient.update', $patient->id]]) !!}
	<div class="panel panel-default">
		<div class="panel-heading">
			Edit Staff Profile
		</div>

		<div class="panel-body">
			@include('patient.fields')
			{!! Form::submit("Edit Patient", ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
	</div>

@stop

