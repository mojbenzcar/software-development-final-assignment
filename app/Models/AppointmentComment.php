<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AppointmentComment extends Model
{
    protected $guarded = [];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
