@extends('layouts.app')
@section('page_title',"Appointment Management")
@section('content')
	{!! Form::open(['method' => 'POST', 'route' => ['appointment.store']]) !!}

	<div class="panel panel-default">
		<div class="panel-heading">
			Create new appointment
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('patient_id', 'Patient*', ['class' => 'control-label']) !!}
					@php
						$patient_id = old('patient_id');

					if(request()->has('patient_id')){
						$patient_id = request()->get('patient_id');
					}
					@endphp
					{!! Form::select('patient_id', $patients, $patient_id, ['class' => 'form-control', 'required'
					 => '']) !!}

					<p class="help-block"></p>
					@if($errors->has('patient_id'))
						<p class="help-block">
							{{ $errors->first('patient_id') }}
						</p>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 form-group">
					@php
						$roster_id = old('roster_id');

					if(request()->has('roster_id')){
						$roster_id = request()->get('roster_id');
					}
					@endphp
					{!! Form::label('roster_id', 'Doctor Schedule*', ['class' => 'control-label']) !!}
					{!! Form::select('roster_id', $rosters, $roster_id, ['class' => 'form-control', 'required'
					 => '']) !!}

					@if($errors->has('roster_id'))
						<p class="help-block">
							{{ $errors->first('roster_id') }}
						</p>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('start_time', 'Start time*', ['class' => 'control-label']) !!}
					{!! Form::text('start_time', old('start_time'), ['class' => 'form-control timepicker', 'placeholder' => '', 'required' => '']) !!}
					<p class="help-block"></p>
					@if($errors->has('start_time'))
						<p class="help-block">
							{{ $errors->first('start_time') }}
						</p>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('end_time', 'Finish time', ['class' => 'control-label']) !!}
					{!! Form::text('end_time', old('end_time'), ['class' => 'form-control timepicker', 'placeholder' =>
					'']) !!}
					<p class="help-block"></p>
					@if($errors->has('end_time'))
						<p class="help-block">
							{{ $errors->first('end_time') }}
						</p>
					@endif
				</div>
			</div>


			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('comments', 'Comments', ['class' => 'control-label']) !!}
					{!! Form::textarea('comments', old('comments'), ['class' => 'form-control ', 'placeholder' => '']) !!}
					<p class="help-block"></p>
					@if($errors->has('comments'))
						<p class="help-block">
							{{ $errors->first('comments') }}
						</p>
					@endif
				</div>
			</div>
			{!! Form::submit('Create Appointment', ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
	</div>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css">

@stop

@section('javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.js"></script>
	<script>
        $('.timepicker').timepicker({'timeFormat': 'H:i:s'});

	</script>

@stop