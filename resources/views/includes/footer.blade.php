<footer class="app-footer">
	<div>
		<strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} CQUniversity</strong>
		@lang('strings.backend.general.all_rights_reserved')
	</div>
</footer>
@include('includes.footer_scripts')