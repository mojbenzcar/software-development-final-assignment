@extends('layouts.app')
@section('page_title')
	Staff Roster Management
@endsection
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			Roster Detail
			<div class="actions">
				<a href="{{ route('appointment.create', ['roster_id'=>$roster->id]) }}"> <i class="fa fa-plus"></i>
					Create
					Appointment</a>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-md-9">
					<table class="table table-bordered table-striped">
						<tr>
							<th>Doctor</th>
							<td>{{ $roster->staff->name }}</td>
						</tr>

						<tr>
							<th>DateTime</th>
							<td>{{ $roster->date }} ({{ $roster->start_time }} - {{ $roster->end_time }})</td>
						</tr>
					</table>
				</div>
			</div><!-- Nav tabs -->
			<table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>Sno</th>
					<th>Patient</th>
					<th>Time</th>
				</tr>
				</thead>

				<tbody>
				@forelse($roster->appointments()->get() as $appointment)
					<tr>
						<td>{{$appointment->id}}</td>
						<td>{{$appointment->patient->name}}</td>
						<td>({{$roster->start_time}} - {{$roster->end_time}})</td>
					</tr>
				@empty
					<tr>
						<td colspan="3">No appointments</td>
					</tr>
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
@stop
