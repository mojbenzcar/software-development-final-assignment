@extends('layouts.app')
@section('page_title', "Patient Management")
@section('content')
	{!! Form::open(['method' => 'POST', 'route' => ['patient.store']]) !!}

	<div class="panel panel-default">
		<div class="panel-heading">
			Create New Patient
		</div>

		<div class="panel-body">
			@include('patient.fields')
			{!! Form::submit("Create New Patient", ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>

	</div>

@stop

