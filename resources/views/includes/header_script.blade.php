
<!-- Bootstrap Core CSS -->
<link href="{{url('vendor/bootstrap/css')}}/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="{{url('vendor/metisMenu')}}/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="{{url('css')}}/sb-admin-2.css" rel="stylesheet">
<!-- DataTables CSS -->
<link href="{{url('vendor/datatables-plugins')}}/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{url('vendor/datatables-responsive')}}/dataTables.responsive.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="{{url('vendor/font-awesome/css')}}/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->