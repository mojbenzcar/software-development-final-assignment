<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAppointmentRequest;
use App\Models\Appointment;
use App\Models\Patient;
use App\Models\Roster;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    protected $roster;
    protected $appointment;

    public function __construct(Roster $roster, Appointment $appointment)
    {
        $this->roster      = $roster;
        $this->appointment = $appointment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointmentModel = $this->appointment;
        $rosterModel      = $this->roster;

        return view('appointment.index', compact('appointmentModel', 'rosterModel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'rosters'  => $this->roster->upcoming()->get()->pluck('identifier', 'id')->prepend('Please select', ''),
            'patients' => Patient::get()->pluck('identifier', 'id')->prepend('Please select', ''),
        ];

        return view('appointment.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAppointmentRequest $request)
    {
        $data            = $request->all();
        $roster          = Roster::find($request->roster_id);
        $data['user_id'] = $roster->user_id;
        $data['date']    = $roster->date;

        $appointment = Appointment::create($data);
        if (!$appointment) {
            session()->flash('error', "Error creating Appointment. Please contact tech department.");

            return redirect()->back();
        }
        session()->flash('success', "Appointment Created Successfully");

        return redirect()->route('appointment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment $appointment
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment $appointment
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Appointment         $appointment
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment $appointment
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
