@extends('layouts.app')
@section('page_title',"Nurse Task Management")
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			List of Task
			@if (auth()->user()->isAdmin())
				<div class="actions">
					<a href="{{ route('task.create') }}"><i class="glyphicon glyphicon-plus-sign"></i> Add New
						Task</a>
				</div>

			@endif
		</div>

		<div class="panel-body table-responsive">
			<table class="table table-bordered table-striped" id="datatable">
				<thead>
				<tr>
					<th>#Id</th>
					<th>Nurse</th>
					<th>Task</th>
					<th>Status</th>
					<th>Create AT</th>
				</tr>
				</thead>

				<tbody>

				@forelse ($tasks as $task)
					<tr>
						<td>{{ $task->id }}</td>
						<td>{{ $task->nurse->name }}</td>
						<td>{{ $task->task }}</td>
						<td>{{ $task->status }}</td>
						<td>{{ $task->created_at }}</td>
						<td>
							<a href="{{ route('task.edit',[$task->id]) }}"
							   class="btn btn-xs btn-info">Edit</a>
							@if (auth()->user()->isAdmin())
								{!! \Form::open(array(
									'style' => 'display: inline-block;',
									'method' => 'DELETE',
									'onSubmit' => "return confirm('Are you sure');",
									'route' => ['task.destroy', $task->id])) !!}
								{!! \Form::submit("Delete", array('class' => 'btn btn-xs btn-danger')) !!}
								{!! \Form::close() !!}
							@endif

						</td>
					</tr>
				@empty
					<tr>
						<td colspan="9">No Data</td>
					</tr>
				@endforelse

				</tbody>
			</table>
		</div>
	</div>
@stop