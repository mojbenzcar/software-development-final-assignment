@extends('layouts.app')
@section('page_title', "Staff Management")
@section('content')
	{!! Form::open(['method' => 'POST', 'route' => ['user.store']]) !!}

	<div class="panel panel-default">
		<div class="panel-heading">
			Create New Staff Profile
		</div>

		<div class="panel-body">
			@include('staff.fields')
			{!! Form::submit("Create New Staff", ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>

	</div>

@stop

