@extends('layouts.app')
@section('page_title',"Appointment Management")
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			@if(auth()->user()->isReceptionist())
			<a href="{{route('appointment.create')}}" class="btn btn-success"><i class="fa fa-plus-circle"></i> New
				Appointment</a>

			<a class="btn btn-primary" href="{{ route('patient.create') }}"><i class="glyphicon
			glyphicon-plus-sign"></i> Add New
				Patient</a>
			@endif	

		</div>
		<div class="panel-body table-responsive">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#today_appointment" aria-controls="today_appointment" role="tab"
					   data-toggle="tab">Today's Appointments</a></li>
				<li role="presentation"><a href="#upcoming_appointments" aria-controls="tests" role="tab"
										   data-toggle="tab">Upcoming Appointments</a></li>
				<li role="presentation"><a href="#upcoming_appointment_dates" aria-controls="tests" role="tab"
										   data-toggle="tab">Available Appointment dates</a></li>
				<li role="presentation"><a href="#past_appointments" aria-controls="tests" role="tab"
										   data-toggle="tab">Past Appointments</a></li>

			</ul>

			<!-- Tab panes -->
			<div class="tab-content">

				<div role="tabpanel" class="tab-pane active" id="today_appointment">
					<table class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>#SN</th>
							<th>Doctor</th>
							<th>Patient</th>
							<th>Time</th>
						</tr>
						</thead>

						<tbody>
						@forelse($appointmentModel->todays()->get() as $appointment)
							<tr>
								<td>{{$appointment->id}}</td>
								<td>{{$appointment->doctor->name}}</td>
								<td><a href="{{route('patient.show',$appointment->patient->id )}}">{{$appointment->patient->name}}</a></td>
								<td>({{$appointment->start_time}} - {{$appointment->end_time}})</td>

								<td><a href="{{route('patient.comment.create', [$appointment->patient->id,
								'appointment_id'=>$appointment->id])}}"
									   class="btn
								btn-success">Add
										Notes</a></td>

							</tr>
						@empty
							<tr>
								<td colspan="4">No appointments for today</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>

				<div role="tabpanel" class="tab-pane" id="upcoming_appointments">
					<table class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>#SN</th>
							<th>Doctor</th>
							<th>Patient</th>
							<th>Time</th>
						</tr>
						</thead>

						<tbody>
						@forelse($appointmentModel->upcoming()->get() as $appointment)
							<tr>
								<td>{{$appointment->id}}</td>
								<td>{{$appointment->doctor->name}}</td>
								<td>{{$appointment->patient->name}}</td>
								<td>{{$appointment->date}} ({{$appointment->start_time}} - {{$appointment->end_time}})
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="4">No appointments for today</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>

				<div role="tabpanel" class="tab-pane" id="upcoming_appointment_dates">
					<table class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>Sno</th>
							<th>Doctor</th>
							<th>DateTime</th>
							<th>Book</th>
						</tr>
						</thead>

						<tbody>
						@forelse($rosterModel->upcoming()->get() as $roster)
							<tr>
								<td>{{$roster->id}}</td>
								<td>{{$roster->staff->name}}</td>
								<td>{{$roster->date}} ({{$roster->start_time}} - {{$roster->end_time}})</td>

								<td>
									<a href="{{route('roster.show', $roster->id)}}"> Detail</a>/
									<a href="{{route('appointment.create',['roster_id'=>$roster->id])}}">Book</a>
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="4">No appointments for today</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>

				<div role="tabpanel" class="tab-pane" id="past_appointments">
					<table class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>Sno</th>
							<th>Doctor</th>
							<th>Patients</th>
							<th>Date</th>
						</tr>
						</thead>

						<tbody>
						@forelse($appointmentModel->past()->get() as $roster)
							<tr>
								<td>{{$appointment->id}}</td>
								<td>{{$appointment->doctor->name}}</td>
								<td>{{$appointment->patient->name}}</td>
								<td>{{$appointment->date}} ({{$appointment->start_time}} - {{$appointment->end_time}})
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="4">No appointments</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					...
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@stop
