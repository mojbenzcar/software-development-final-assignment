@extends('layouts.app')
@section('page_title', "Nurse Task Management")
@section('content')
	{!! Form::model($task, ['method' => 'PUT', 'route' => ['task.update', $task->id]]) !!}
	<div class="panel panel-default">
		<div class="panel-heading">
			Nurse Task Edit
		</div>

		<div class="panel-body">
			@include('task.fields')

			@if (auth()->user()->isNurse())
				<div class="col-xs-12 form-group">
					{!! Form::label('comment', 'Comment', ['class' => 'control-label']) !!}
					{!! Form::textArea('comment', old('comment'), ['class' => 'form-control', 'placeholder' => '',
					'required' =>
					'']) !!}
					<p class="help-block"></p>
					@if($errors->has('comment'))
						<p class="help-block">
							{{ $errors->first('comment') }}
						</p>
					@endif
				</div>
				<div class="row">
					<div class="col-xs-12 form-group">
						{!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
						{!! Form::select('status', ['pending'=>"Pending",'ongoing'=>"Ongoing",'complete'=>"Complete"], old('status'), ['class' => 'form-control',
						'required'
						=> ''])
						 !!}
						@if($errors->has('status'))
							<p class="help-block">
								{{ $errors->first('status') }}
							</p>
						@endif
					</div>
				</div>
			@endif
			{!! Form::submit("Update", ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
	</div>

@stop

