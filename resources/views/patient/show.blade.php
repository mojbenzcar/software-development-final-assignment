@extends('layouts.app')
@section("page_title","Patient detail")
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			{{$patient->name}}
			<div class="actions">
				<a href="{{route('appointment.create', ['patient_id'=>$patient->id])}}">Add Appointment</a>/
				<a href="{{route('patient.comment.create', $patient->id)}}">Add Comment</a>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-md-9">
					<table class="table table-bordered table-striped">
						<tr>
							<th>Name</th>
							<td>{{ $patient->name }}</td>
						</tr>
						<tr>
							<th>Address</th>
							<td>{{ $patient->address }}</td>
						</tr>
						<tr>
							<th>Phone</th>
							<td>{{ $patient->phone }}</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>{{ $patient->email }}</td>
						</tr>
						<tr>
							<th>DOB</th>
							<td>{{ $patient->dob }}</td>
						</tr>
						<tr>
							<th>Patient History</th>
							<td>{{ $patient->history }}</td>
						</tr>
					</table>
				</div>
			</div><!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">

				<li role="presentation" class="active"><a href="#appointments" aria-controls="appointments" role="tab"
														  data-toggle="tab">Appointments</a></li>
				<li role="presentation"><a href="#findings" aria-controls="findings" role="tab"
										   data-toggle="tab">Finding/Comments</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">

				<div role="tabpanel" class="tab-pane active" id="appointments">
					<table class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>Doctor</th>
							<th>Datetime</th>
						</tr>
						</thead>

						<tbody>

						@forelse($patient->appointments()->get() as $appointment)
							<tr>
								<td>{{ $appointment->doctor->name }}</td>
								<td>{{$appointment->date}} ({{$appointment->start_time}} - {{$appointment->end_time}})
								</td>
								<td><a href="{{route('patient.comment.create', [$patient->id,
								'appointment_id'=>$appointment->id])}}"
									   class="btn
								btn-success">Add
										Comment</a></td>
							</tr>
						@empty
						@endforelse
						</tbody>
					</table>
				</div>
				<div role="tabpanel" class="tab-pane" id="findings">

					<table class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>Comment/finding</th>
							<th>Added By</th>
							<th>Added Date</th>
							<th></th>
						</tr>
						</thead>

						<tbody>

						@forelse($patient->comments()->get() as $comment)
							<tr>
								<td>{{ $comment->comments }}</td>
								<td>{{$comment->createdBy->name}}
								</td>
								<td>{{$comment->created_at}}
								</td>
								<td><a href="{{route('patient.comment.edit', [$patient->id, $comment->id])}}" class="btn
								btn-success
								btn-sm">
										Edit</a>
								</td>
							</tr>
						@empty
						@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop