@extends('layouts.app')
@section("page_title","Staff Roster Management")
@section('content')
	{!! Form::model($roster, ['method' => 'PUT', 'route' => ['roster.update', $roster->id]]) !!}

	<div class="panel panel-default">
		<div class="panel-heading">
			Edit Roster
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('user_id', 'Staff*', ['class' => 'control-label']) !!}
					{!! Form::select('user_id', $users, old('user_id'), ['class' => 'form-control', 'required' => ''])
					 !!}
					@if($errors->has('user_id'))
						<p class="help-block">
							{{ $errors->first('user_id') }}
						</p>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('date', 'Date*', ['class' => 'control-label']) !!}
					{!! Form::text('date', old('date'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
					@if($errors->has('date'))
						<p class="help-block">
							{{ $errors->first('date') }}
						</p>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('start_time', 'Start time*', ['class' => 'control-label']) !!}
					{!! Form::text('start_time', old('start_time'), ['class' => 'form-control timepicker', 'placeholder' => '', 'required' => '']) !!}
					<p class="help-block"></p>
					@if($errors->has('start_time'))
						<p class="help-block">
							{{ $errors->first('start_time') }}
						</p>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					{!! Form::label('end_time', 'Finish time', ['class' => 'control-label']) !!}
					{!! Form::text('end_time', old('end_time'), ['class' => 'form-control timepicker', 'placeholder' =>
					'']) !!}
					<p class="help-block"></p>
					@if($errors->has('end_time'))
						<p class="help-block">
							{{ $errors->first('end_time') }}
						</p>
					@endif
				</div>
			</div>

			{!! Form::submit('Edit Roster', ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
	</div>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css">


@stop

@section('javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.js"></script>
	<script>

        $('.date').datepicker({
            autoclose: true,
            dateFormat: "{{ config('app.date_format_js') }}",
            minDate:  new Date()
        });

        $('.timepicker').timepicker({ 'timeFormat': 'H:i:s' });

	</script>

@stop