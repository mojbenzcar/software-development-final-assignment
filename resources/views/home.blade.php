@extends('layouts.app')
@section('page_title', "Dashboard")
@section('content')
	@if(auth()->user()->isAdmin())
		@include("dashboard.admin")
	@endif
	@if(auth()->user()->isDoctor())
		@include("dashboard.doctor")
	@endif
	@if(auth()->user()->isReceptionist())
		@include("dashboard.receptionist")
	@endif
	@if(auth()->user()->isNurse())
		@include("dashboard.nurse")
	@endif

@endsection
