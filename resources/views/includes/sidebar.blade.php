@inject('request', 'Illuminate\Http\Request')

<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">

			<li>
				<a href="{{route('home')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
			</li>
			@if(auth()->user()->isAdmin())
				@include("sidebar.admin")
			@endif
			@if(auth()->user()->isDoctor())
				@include("sidebar.doctor")
			@endif
			@if(auth()->user()->isReceptionist())
				@include("sidebar.receptionist")
			@endif
			@if(auth()->user()->isNurse())
				@include("sidebar.nurse")
			@endif
			<li>
				<a href="#logout" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
					<i class="fa fa-arrow-left"></i>
					<span class="title">Logout</span>
				</a>
			</li>
			<li class="fill" style="height: 400px"></li>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>