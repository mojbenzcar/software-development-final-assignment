<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{
    protected $fillable = ['date', 'start_time', 'end_time', 'user_id'];

    /**
     * Get attribute from date format
     *
     * @param $input
     *
     * @return string
     */
    public function getDateAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to date format
     *
     * @param $input
     */
    public function setStartTimeAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['start_time'] = Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            $this->attributes['start_time'] = null;
        }
    }

    /**
     * Get attribute from date format
     *
     * @param $input
     *
     * @return string
     */
    public function getStartTimeAttribute($input)
    {
        if ($input != null && $input != '') {
            return Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            return '';
        }
    }

    /**
     * Set attribute to date format
     *
     * @param $input
     */
    public function setEndTimeAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['end_time'] = Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            $this->attributes['end_time'] = null;
        }
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get attribute from date format
     *
     * @param $input
     *
     * @return string
     */
    public function getEndTimeAttribute($input)
    {
        if ($input != null && $input != '') {
            return Carbon::createFromFormat('H:i:s', $input)->format('H:i:s');
        } else {
            return '';
        }
    }

    public function upcoming($role = 3)
    {
        return $this->whereHas(
            'staff',
            function ($q) use ($role) {
                $q->where('role_id', $role);
            }
        )->whereDate(
            'date',
            '>=',
            Carbon::today()->toDateString()
        )->orderBy('date');
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getIdentifierAttribute()
    {
        return "{$this->staff->name} {$this->date}({$this->start_time}-{$this->end_time})";
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'roster_id');
    }

}
