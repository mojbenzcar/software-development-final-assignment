<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'appointments',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('patient_id')->unsigned()->nullable();
                $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete(
                    'cascade'
                );
                $table->integer('roster_id')->unsigned()->nullable();
                $table->foreign('roster_id')->references('id')->on('rosters')->onDelete(
                    'cascade'
                );
                $table->date('date');
                $table->time('start_time');
                $table->time('end_time')->nullable();
                $table->string('status')->default('entered');
                $table->text('comments')->nullable();

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
