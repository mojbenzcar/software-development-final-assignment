@extends('layouts.app')
@section('page_title')
	Staff Roster Management
@endsection
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			Roster list
			<div class="actions">
				<a href="{{ route('roster.create') }}"> <i class="fa fa-plus"></i> Create New Roster</a>
			</div>
		</div>

		<div class="panel-body">
			<div id='calendar'></div>
		</div>
	</div>
@stop

@section('javascript')
	<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css'/>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js'></script>
	<script>
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                defaultView: 'agendaWeek',
                events: [
						@foreach($rosters as $roster)
                    {
                        title: '{{ $roster->staff->name }}',
                        start: '{{ $roster->date . ' ' . $roster->start_time }}',
                        end: '{{ $roster->date . ' ' . $roster->end_time }}',
                        url: '{{ route('roster.edit', $roster->id) }}'
                    },
					@endforeach
                ]
            })
        });
	</script>
@endsection