<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function __construct()
    {
        if (!Gate::allows('staff_management')) {
            //return abort(401);
        }
    }

    /**
     * Display a listing of Staff.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('role')->orderBy('email')->get();

        return view('staff.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = [
            'roles' => Role::get()->pluck('title', 'id')->prepend('Please select Staff Type', ''),
        ];

        return view('staff.create', $roles);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $user = User::create($request->all());
        if (!$user) {
            session()->flash('error', "Error creating Staff. Please contact tech department.");

            return redirect()->back();
        }
        session()->flash('success', "Staff Created Successfully");

        return redirect()->route('user.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $relations = [
            'roles' => Role::get()->pluck('title', 'id')->prepend('Please select', ''),
            'user'  => User::findOrFail($id),
        ];


        return view('staff.edit', $relations);
    }

    /**
     * write brief description
     *
     * @param User            $user
     * @param EditUserRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(User $user, EditUserRequest $request)
    {
        try {
            if (!empty($request->password)) {
                $user->update($request->all());
            } else {
                $user->update($request->except('password'));
            }
            session()->flash('success', "Staff information edited Successfully.");

        } catch (\Exception $e) {
            session()->flash('error', "Error updating staff information.");
        }

        return redirect()->route('user.index');
    }


    /**
     * Display User.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $relations = [
            'roles' => Role::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $user = User::findOrFail($id);

        return view('staff.show', compact('user') + $relations);
    }


    /**
     * Remove User from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('staff.index');
    }

}
