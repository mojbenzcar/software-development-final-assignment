<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('role_id', 'Staff Type*', ['class' => 'control-label']) !!}
		{!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control', 'required' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('role_id'))
			<p class="help-block">
				{{ $errors->first('role_id') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
		{!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('name'))
			<p class="help-block">
				{{ $errors->first('name') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
		{!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('email'))
			<p class="help-block">
				{{ $errors->first('email') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('password', 'Password*', ['class' => 'control-label']) !!}
		{!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('password'))
			<p class="help-block">
				{{ $errors->first('password') }}
			</p>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('mobile_no', 'Contact Number*', ['class' => 'control-label']) !!}
		{!! Form::text('mobile_no', old('mobile_no'), ['class' => 'form-control', 'placeholder' => '', 'required' =>
		''])
		 !!}
		<p class="help-block"></p>
		@if($errors->has('mobile_no'))
			<p class="help-block">
				{{ $errors->first('mobile_no') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
		{!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '', 'required' =>
		''])
		 !!}
		<p class="help-block"></p>
		@if($errors->has('address'))
			<p class="help-block">
				{{ $errors->first('address') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('qualification', 'Qualification*', ['class' => 'control-label']) !!}
		{!! Form::textArea('qualification', old('qualification'), ['class' => 'form-control', 'placeholder' => '',
		'required' =>
		''])
		 !!}
		<p class="help-block"></p>
		@if($errors->has('qualification'))
			<p class="help-block">
				{{ $errors->first('qualification') }}
			</p>
		@endif
	</div>
</div>