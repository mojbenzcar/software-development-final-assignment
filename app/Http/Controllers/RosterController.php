<?php

namespace App\Http\Controllers;


use App\Http\Requests\CreateRosterRequest;
use App\Models\Roster;
use App\User;
use Illuminate\Http\Request;

class RosterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rosters = Roster::with('staff')->get();

        return view('roster.index', compact('rosters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $formData = [
            'users' => User::get()->pluck('name_email', 'id')->prepend('Please select', ''),
        ];

        return view('roster.create', $formData);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRosterRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRosterRequest $request)
    {
        $roster = Roster::create($request->all());
        if ($roster) {
            session()->flash('success', "Roster Created.");
        } else {
            session()->flash('error', "Error Creating Roster.");
        }

        return redirect()->route('roster.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roster $roster
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Roster $roster)
    {
        return view('roster.show', compact('roster'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roster $roster
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Roster $roster)
    {
        $formData = [
            'users'  => User::get()->pluck('name_email', 'id')->prepend('Please select', ''),
            'roster' => $roster,
        ];

        return view('roster.edit', $formData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Roster              $roster
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRosterRequest $request, Roster $roster)
    {
        $roster = $roster->update($request->all());
        if ($roster) {
            session()->flash('success', "Roster Update.");
        } else {
            session()->flash('error', "Error Updating Roster.");
        }

        return redirect()->route('roster.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roster $roster
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roster $roster)
    {
        //
    }
}
