@extends('layouts.app')
@section('page_title', "Staff Management")
@section('content')
	{!! Form::model($user, ['method' => 'PUT', 'route' => ['user.update', $user->id]]) !!}
	<div class="panel panel-default">
		<div class="panel-heading">
			Edit Staff Profile
		</div>

		<div class="panel-body">
			@include('staff.fields')
			{!! Form::submit("Edit Staff", ['class' => 'btn btn-primary']) !!}
			{!! Form::close() !!}
		</div>
	</div>

@stop

