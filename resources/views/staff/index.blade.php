@extends('layouts.app')
@section('page_title',"Staff Management")
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			List of Staff
			<div class="actions">
				<a href="{{ route('user.create') }}"><i class="glyphicon glyphicon-plus-sign"></i> Add New Staff</a>
			</div>
		</div>

		<div class="panel-body table-responsive">
			<table class="table table-bordered table-striped" id="datatable">
				<thead>
				<tr>
					<th>#Id</th>
					<th>Name</th>
					<th>Email</th>
					<th>Role</th>
					<th>&nbsp;</th>
				</tr>
				</thead>

				<tbody>

				@forelse ($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->role->title}}</td>
						<td>
							<a href="{{ route('user.edit',[$user->id]) }}"
							   class="btn btn-xs btn-info">Edit</a>
							{!! \Form::open(array(
								'style' => 'display: inline-block;',
								'method' => 'DELETE',
								'onSubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
								'route' => ['user.destroy', $user->id])) !!}
							{!! \Form::submit("Delete", array('class' => 'btn btn-xs btn-danger')) !!}
							{!! \Form::close() !!}

						</td>
					</tr>
				@empty
					<tr>
						<td colspan="9">No Data</td>
					</tr>
				@endforelse

				</tbody>
			</table>
		</div>
	</div>
@stop