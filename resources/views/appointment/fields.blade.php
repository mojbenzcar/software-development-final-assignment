<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
		{!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('name'))
			<p class="help-block">
				{{ $errors->first('name') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
		{!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('email'))
			<p class="help-block">
				{{ $errors->first('email') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('phone', 'Contact Number*', ['class' => 'control-label']) !!}
		{!! Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => '', 'required' =>
		''])
		 !!}
		<p class="help-block"></p>
		@if($errors->has('phone'))
			<p class="help-block">
				{{ $errors->first('phone') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
		{!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '', 'required' =>
		''])
		 !!}
		<p class="help-block"></p>
		@if($errors->has('address'))
			<p class="help-block">
				{{ $errors->first('address') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('dob', 'DOB', ['class' => 'control-label']) !!}
		{!! Form::text('dob', old('dob'), ['class' => 'form-control', 'placeholder' => '', 'required' =>
		''])
		 !!}
		<p class="help-block"></p>
		@if($errors->has('dob'))
			<p class="help-block">
				{{ $errors->first('dob') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('history', 'Patient History', ['class' => 'control-label']) !!}
		{!! Form::textArea('history', old('history'), ['class' => 'form-control', 'placeholder' => '',
		'required' =>
		''])
		 !!}
		<p class="help-block"></p>
		@if($errors->has('history'))
			<p class="help-block">
				{{ $errors->first('history') }}
			</p>
		@endif
	</div>
</div>