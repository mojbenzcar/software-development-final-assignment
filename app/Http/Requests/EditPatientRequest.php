<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditPatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       // dd($this->route('patient')->id);
        return [
            'name'    => 'required',
            'phone'   => 'required',
            'address' => 'required',
            'email'   => 'sometimes|email|unique:patients,email,'.$this->route('patient')->id,
        ];
    }
}
