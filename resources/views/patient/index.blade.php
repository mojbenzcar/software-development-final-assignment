@extends('layouts.app')
@section('page_title',"Patient Management")
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			List of Patient
			<div class="actions">
				<a href="{{ route('patient.create') }}"><i class="glyphicon glyphicon-plus-sign"></i> Add New
					Patient</a>
			</div>
		</div>

		<div class="panel-body table-responsive">
			<table class="table table-bordered table-striped" id="datatable">
				<thead>
				<tr>
					<th>#Id</th>
					<th>Name</th>
					<th>Phone</th>
					<th>&nbsp;</th>
				</tr>
				</thead>

				<tbody>

				@forelse ($patients as $patient)
					<tr>
						<td>{{ $patient->id }}</td>
						<td><a href="{{ route('patient.show',[$patient->id]) }}"
							   class="">{{ $patient->name }}</a></td>
						<td>{{ $patient->phone }}</td>
						<td>
							<a href="{{ route('patient.edit',[$patient->id]) }}"
							   class="btn btn-xs btn-info">Edit</a>
							{!! \Form::open(array(
								'style' => 'display: inline-block;',
								'method' => 'DELETE',
								'onSubmit' => "return confirm('Are you sure');",
								'route' => ['patient.destroy', $patient->id])) !!}
							{!! \Form::submit("Delete", array('class' => 'btn btn-xs btn-danger')) !!}
							{!! \Form::close() !!}

						</td>
					</tr>
				@empty
					<tr>
						<td colspan="9">No Data</td>
					</tr>
				@endforelse

				</tbody>
			</table>
		</div>
	</div>
@stop