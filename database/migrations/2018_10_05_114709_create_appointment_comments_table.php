<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'appointment_comments',
            function (Blueprint $table) {
                $table->increments('id');

                $table->integer('appointment_id')->unsigned()->nullable();
                $table->foreign('appointment_id')->references('id')->on('appointments')->onDelete(
                    'cascade'
                );

                $table->integer('patient_id')->unsigned()->nullable();
                $table->foreign('patient_id')->references('id')->on('patients')->onDelete(
                    'cascade'
                );

                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete(
                    'cascade'
                );

                $table->text('comments')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_comments');
    }
}
