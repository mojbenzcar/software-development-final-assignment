<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('task', 'Task*', ['class' => 'control-label']) !!}
		{!! Form::textArea('task', old('task'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
		<p class="help-block"></p>
		@if($errors->has('task'))
			<p class="help-block">
				{{ $errors->first('task') }}
			</p>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{!! Form::label('user_id', 'Staff*', ['class' => 'control-label']) !!}
		{!! Form::select('user_id', $users, old('user_id'), ['class' => 'form-control', 'required' => ''])
		 !!}
		@if($errors->has('user_id'))
			<p class="help-block">
				{{ $errors->first('user_id') }}
			</p>
		@endif
	</div>
</div>